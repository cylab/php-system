<?php

namespace Cylab\System;

use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class System
{

    private $meminfo = null;

    public function listNetworkInterfaces() : array
    {
        $process = new Process(['ip', 'addr']);
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        return $this->parseInterfaces($process->getOutput());
    }

    const INTERFACES_REGEXP = '/^\d+: (.*):/m';

    public function parseInterfaces(string $string) : array
    {
        $values = [];
        preg_match_all(self::INTERFACES_REGEXP, $string, $values);

        $interfaces = [];
        $count = count($values[1]);
        for ($i = 0; $i < $count; $i++) {
            $interfaces[] = $values[1][$i];
        }
        return $interfaces;
    }

    public function findAvailablePort(int $min, int $max) : int
    {
        $list = $this->listListeningPorts();
        $ports = [];
        foreach ($list as $p) {
            $ports[] = $p->port;
        }

        while (true) {
            $p = \rand($min, $max);
            if (!in_array($p, $ports)) {
                return $p;
            }
        }
    }

    public function listListeningPorts() : array
    {
        $process = new Process(['netstat', '-anp']);
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $string = $process->getOutput();
        return $this->parseNetstat($string);
    }

    const NETSTAT_REGEXP = "/(tcp6|tcp|udp6|udp)\s*\d\s*\d\s*(\S*):(\d*).*LISTEN\s*(\S*)/m";

    public function parseNetstat(string $string) : array
    {
        $values = [];
        preg_match_all(self::NETSTAT_REGEXP, $string, $values);

        $ports = [];
        $count = count($values[1]);
        for ($i = 0; $i < $count; $i++) {
            $port = new ListeningPort();
            $port->proto = $values[1][$i];
            $port->bind = $values[2][$i];
            $port->port = $values[3][$i];
            $port->process = $values[4][$i];
            $ports[] = $port;
        }
        return $ports;
    }

    public function load1() : float
    {
        return (float) sys_getloadavg()[0];
    }

    public function load5() : float
    {
        return (float) sys_getloadavg()[1];
    }

    public function load15() : float
    {
        return (float) sys_getloadavg()[2];
    }

    /**
     * Get the number of vCores.
     *
     * @return int
     * @throws ProcessFailedException
     */
    public function vcores() : int
    {
        $process = new Process(['grep', '-c', 'processor', '/proc/cpuinfo']);
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        return (int) $process->getOutput();
    }

    public function meminfo() : string
    {
        if ($this->meminfo == null) {
            $this->meminfo = file_get_contents("/proc/meminfo");
        }

        return $this->meminfo;
    }

    private function matchOne(string $re, string $str)
    {
        $matches = [];
        preg_match($re, $str, $matches, PREG_OFFSET_CAPTURE, 0);
        return $matches[1][0];
    }

    const MEMTOTAL = '/^MemTotal:\s*(\d*) kB/m';

    /**
     * Get the total RAM memory in kB.
     * @return int
     */
    public function memTotal() : int
    {
        return $this->matchOne(self::MEMTOTAL, $this->meminfo());
    }

    const MEMFREE = '/^MemFree:\s*(\d*) kB/m';

    /**
     * Free memory, in kB.
     * @return int
     */
    public function memFree() : int
    {
        return $this->matchOne(self::MEMFREE, $this->meminfo());
    }

    /**
     * Total used memory, in kB.
     * @return int
     */
    public function memUsed() : int
    {
        return $this->memTotal() - $this->memAvailable();
    }

    const MEMAVAILABLE = '/^MemAvailable:\s*(\d*) kB/m';

    public function memAvailable() : int
    {
        return $this->matchOne(self::MEMAVAILABLE, $this->meminfo());
    }
}
