<?php

namespace Cylab\System;

/**
 * Represents a single listening port
 *
 * @author tibo
 */
class ListeningPort
{
    public $port;
    public $proto;
    public $bind;
    public $process;
}
