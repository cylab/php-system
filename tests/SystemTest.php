<?php

namespace Cylab\System;

class SystemTest extends \PHPUnit\Framework\TestCase
{

    public function testBasic()
    {
        $this->assertTrue(true);
    }

    /**
     * @group interfaces
     */
    public function testParseInterfaces()
    {
        $string = file_get_contents(__DIR__ . '/ipaddr.txt');
        $s = new System();
        $interfaces = $s->parseInterfaces($string);
        $this->assertEquals(8, count($interfaces));
        $this->assertEquals("enp0s31f6", $interfaces[1]);
    }

    public function testParseNetstat()
    {
        $string = file_get_contents(__DIR__ . "/netstat.txt");
        $s = new System();
        $p = $s->parseNetstat($string);

        $this->assertEquals(11, count($p));
        $this->assertEquals(5939, $p[0]->port);
    }

    public function testListListeningPorts()
    {
        $s = new System();
        $this->assertTrue(is_array($s->listListeningPorts()));
    }

    public function testFindAvailablePort()
    {
        $s = new System();
        $p = $s->findAvailablePort(2200, 2299);
        $this->assertTrue($p >= 2200);
        $this->assertTrue($p <= 2299);
    }

    public function testVcores()
    {
        $s = new System();
        $this->assertTrue($s->vcores() > 0);
    }

    public function testLoad1()
    {
        $s = new System();
        $this->assertTrue($s->load1() > 0);
    }

    public function testLoad5()
    {
        $s = new System();
        $this->assertTrue($s->load5() > 0);
    }

    public function testLoad15()
    {
        $s = new System();
        $this->assertTrue($s->load15() > 0);
    }

    public function testMemFree()
    {
        $s = new System();
        var_dump($s->memFree());
        $this->assertTrue($s->load1() > 0);
    }
}
