# cylab/system

[![pipeline status](https://gitlab.cylab.be/cylab/php-system/badges/master/pipeline.svg)](https://gitlab.cylab.be/cylab/php-system/commits/master)
[![coverage report](https://gitlab.cylab.be/cylab/php-system/badges/master/coverage.svg)](https://gitlab.cylab.be/cylab/php-system/commits/master)

Easily retrieve system information like CPU and RAM usage or listening ports.

For example:

```php
use Cylab\System\System;

$s = new System();

// CPU & Load
var_dump($s->vcores());
var_dump($s->load1());
var_dump($s->load5());
var_dump($s->load15());

// Memory (in kB)
var_dump($s->memTotal());
var_dump($s->memUsed());
var_dump($s->memFree());
var_dump($s->memAvailable());

// Network ports
var_dump($s->findAvailablePort(2200, 2299));
var_dump($s->listListeningPorts());
```

## Listening ports

```
var_dump($s->listListeningPorts());
```

Will produce something like:

```
array(3) {
  [0] =>
  class Cylab\System\ListeningPort#50 (4) {
    public $port =>
    string(2) "53"
    public $proto =>
    string(3) "tcp"
    public $bind =>
    string(10) "127.0.0.53"
    public $process =>
    string(1) "-"
  }
  [1] =>
  class Cylab\System\ListeningPort#49 (4) {
    public $port =>
    string(3) "631"
    public $proto =>
    string(3) "tcp"
    public $bind =>
    string(9) "127.0.0.1"
    public $process =>
    string(1) "-"
  }
  [2] =>
  class Cylab\System\ListeningPort#43 (4) {
    public $port =>
    string(3) "631"
    public $proto =>
    string(4) "tcp6"
    public $bind =>
    string(3) "::1"
    public $process =>
    string(1) "-"
  }
}

```

Or, to find an available port in the range 2200 - 2299 :

```php
$p = $s->findAvailablePort(2200, 2299);
```

## Installation

```bash
composer require cylab/system
```


