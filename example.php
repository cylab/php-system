<?php

require_once __DIR__ . "/vendor/autoload.php";

use Cylab\System\System;

$s = new System();

// CPU & Load
var_dump($s->vcores());
var_dump($s->load1());
var_dump($s->load5());
var_dump($s->load15());

// Memory (in kB)
var_dump($s->memTotal());
var_dump($s->memUsed());
var_dump($s->memFree());
var_dump($s->memAvailable());

// Network interfaces

var_dump($s->listNetworkInterfaces());

// Network ports
var_dump($s->findAvailablePort(2200, 2299));
var_dump($s->listListeningPorts());
